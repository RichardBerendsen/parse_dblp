"""
tag_count.py

just count the occurences of each kind of tag in an xml file, and print to stdout tab separated.

Usage:
    tag_count.py xml_file.gz
"""

import gzip
import sys
import os
import collections
from operator import itemgetter
from lxml import etree

# a target parser class, see
# http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
# for a nice explanation.
class TagCount():
    def __init__(self):
        self.d = collections.defaultdict(lambda: 0)
    def start(self, tag, attrib):
        self.d[tag] += 1
    def end(self, tag):
        pass
    def data(self, tag):
        pass
    def close(self):
        return self.d

def main(argv):
    if len(argv) != 2:
        print >> sys.stderr, __doc__
        return 2

    # load the whole xml file in memory
    path_xml_file_gz = sys.argv[1]
    dir_xml_file_gz = "%s/" % (os.path.dirname(path_xml_file_gz),)
    xml_file_gz = gzip.open(path_xml_file_gz)

    # just parsing the XML in memory is not practical for the entire DBLP xml file.
    # xml_str = xml_file_gz.read()
    ## parse xml
    #parser = etree.XMLParser(load_dtd=True, dtd_validation=True)
    #root = etree.XML(xml_str, parser=parser, base_url=dir_xml_file_gz)
    ## iterate over tree
    #d = collections.defaultdict(lambda: 0)
    #for c in root.iter():
    #    d[c.tag] += 1

    # Therefore, use the target parser method, see
    #   http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    # for a nice exposition.
    # This method does not build a huge parse tree in memory apparently.
    parser = etree.XMLParser(load_dtd=True, dtd_validation=True, target=TagCount())
    d = etree.parse(xml_file_gz, parser=parser, base_url=dir_xml_file_gz)

    # write out number of times each tag appears.
    # sort by number of times appearing, increasing.
    for k, v in sorted(d.iteritems(), key=itemgetter(1)):
        print "%s\t%d" % (k, v)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))


